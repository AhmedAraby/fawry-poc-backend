create database axa_motor_fawry;

use axa_motor_fawry;

create table customer(
	id int not null auto_increment, 
    name varchar(300) not null, 
    phone_number varchar(100) not null, 
    email varchar(200) not null, 
    password varchar(500),
    
    # other data needed by out system
    
    # constraints 
    primary key(id)
    
);

insert into customer values(1, "ahmed araby", "01068482084", "aaraby@sumerge.com", "12345679810");


create table item (
	id int not null auto_increment, 
    description varchar(1024) not null, 
    price decimal not null, 
    quantity decimal not null,
    
    primary key(id)
);

insert into item values (1, "axa motor policy", 1024, 1);

create table transaction(
	id int not null auto_increment,  # this is corresponding to merchentRefNumber in fawry request
    payment_method varchar(100) not null,
    amount varchar(100) not null, 
    payment_expiry timestamp, 
    description varchar(1024) not null, 
    language varchar(16) not null,
    request_signature varchar(1024), 
    
    # data that represent the satate of the transaction
    referenceNumber int, # transaction id in fawry system
    order_amount varchar(100) , 
    payment_amount varchar(100), 
    fawry_fees varchar(100), 
    order_status varchar(100), 
    payment_time timestamp, 
    response_signature varchar(1024), 
    status_code varchar(16),
    status_description varchar(1024), 
    
    primary key(id)
);

select * from customer_transaction;


# create relation tables 
create table customer_transaction(
	customer_id int not null, 
    transaction_id int not null, 
    
    foreign key (customer_id) 
    references customer(id), 
    
    foreign key (transaction_id) 
    references transaction (id)
);


create table item_transaction(
	item_id int not null, 
    transaction_id int not null, 
    
    foreign key(item_id)
    references item(id), 
    
    foreign key(transaction_id)
    references transaction(id)
);
