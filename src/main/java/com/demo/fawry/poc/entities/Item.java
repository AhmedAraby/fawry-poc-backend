package com.demo.fawry.poc.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "item")
public class Item
{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY) // identity will use the DB auto increment
    @Column(name = "id")
    private int id;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private double price;

    @Column(name = "quantity")
    private double quantity;

    @ManyToOne()
    @JoinColumn(name = "transaction_id")
    @JsonIgnore
    private Transaction transaction;

    public Item() {
    }

    public Item(String description, double price, double quantity) {
        this.description = description;
        this.price = price;
        this.quantity = quantity;
    }

    @JsonProperty(value = "itemId")
    public int getId() {
        return id;
    }

    @JsonProperty(value = "itemId")
    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", transaction=" + transaction +
                '}';
    }
}
