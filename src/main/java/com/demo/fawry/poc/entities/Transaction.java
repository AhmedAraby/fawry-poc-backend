package com.demo.fawry.poc.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "transaction")
public class Transaction
{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY) // identity will use the DB auto increment
    @Column(name = "id")
    private int id;

    @Column(name = "payment_method")
    private String paymentMethod;

    @Column(name = "amount")
    private String amount;

    @Column(name = "payment_expiry")
    private Timestamp paymentExpiry;

    @Column(name = "description")
    private String description;

    @Column(name = "language")
    private String language;

    @Column(name = "request_signature")
    private String requestSignature;

    // data from the response

    @Column(name = "reference_number")
    private int referenceNumber;

    @Column(name = "order_amount")
    private String orderAmount;

    @Column(name = "payment_amount")
    private String paymentAmount;

    @Column(name = "fawry_fees")
    private String fawryFees;

    @Column(name = "order_status")
    private String orderStatus;

    @Column(name = "payment_time")
    private Timestamp paymentTime;

    @Column(name = "response_signature")
    private String responseSignature;

    @Column(name = "status_code")
    private String statusCode;

    @Column(name = "status_description")
    private String statusDescription;

    @ManyToOne()
    @JoinColumn(name = "customer_id") // in transaction
    private Customer customer;

    public Transaction() {
    }

    public Transaction(
            String paymentMethod, String amount,
            Timestamp paymentExpiry, String description,
            String language, String requestSignature,
            int referenceNumber, String orderAmount,
            String paymentAmount, String fawryFees,
            String orderStatus, Timestamp paymentTime,
            String responseSignature, String statusCode,
            String statusDescription)
    {
        this.paymentMethod = paymentMethod;
        this.amount = amount;
        this.paymentExpiry = paymentExpiry;
        this.description = description;
        this.language = language;
        this.requestSignature = requestSignature;
        this.referenceNumber = referenceNumber;
        this.orderAmount = orderAmount;
        this.paymentAmount = paymentAmount;
        this.fawryFees = fawryFees;
        this.orderStatus = orderStatus;
        this.paymentTime = paymentTime;
        this.responseSignature = responseSignature;
        this.statusCode = statusCode;
        this.statusDescription = statusDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Timestamp getPaymentExpiry() {
        return paymentExpiry;
    }

    public void setPaymentExpiry(Timestamp paymentExpiry) {
        this.paymentExpiry = paymentExpiry;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRequestSignature() {
        return requestSignature;
    }

    public void setRequestSignature(String requestSignature) {
        this.requestSignature = requestSignature;
    }

    public int getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(int referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getFawryFees() {
        return fawryFees;
    }

    public void setFawryFees(String fawryFees) {
        this.fawryFees = fawryFees;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Timestamp getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Timestamp paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getResponseSignature() {
        return responseSignature;
    }

    public void setResponseSignature(String responseSignature) {
        this.responseSignature = responseSignature;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", paymentMethod='" + paymentMethod + '\'' +
                ", amount='" + amount + '\'' +
                ", paymentExpiry=" + paymentExpiry +
                ", description='" + description + '\'' +
                ", language='" + language + '\'' +
                ", requestSignature='" + requestSignature + '\'' +
                ", referenceNumber=" + referenceNumber +
                ", orderAmount='" + orderAmount + '\'' +
                ", paymentAmount='" + paymentAmount + '\'' +
                ", fawryFees='" + fawryFees + '\'' +
                ", orderStatus='" + orderStatus + '\'' +
                ", paymentTime=" + paymentTime +
                ", responseSignature='" + responseSignature + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", statusDescription='" + statusDescription + '\'' +
                ", customer=" + customer +
                '}';
    }
}
