package com.demo.fawry.poc.models;

public class ReferenceNumberResponseModel
{
    private String referenceNumber;
    private long expiry;

    public ReferenceNumberResponseModel(String referenceNumber, long expiry) {
        this.referenceNumber = referenceNumber;
        this.expiry = expiry;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public long getExpiry() {
        return expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    @Override
    public String toString() {
        return "ReferenceNumberResponseModel{" +
                "referenceNumber='" + referenceNumber + '\'' +
                ", expiry=" + expiry +
                '}';
    }
}
