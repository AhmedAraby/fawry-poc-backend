package com.demo.fawry.poc.models;


/**
 * this class will represent the data comming from
 * the http client talking to our backEnd.
 */

public class ReferenceNumberRequestModel
{
    private String language;
    private String amount;
    private int customerId;

    public ReferenceNumberRequestModel() {
    }

    public ReferenceNumberRequestModel(String language, String amount, int customerId) {
        this.language = language;
        this.amount = amount;
        this.customerId = customerId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "ReferenceNumberRequestModel{" +
                "language='" + language + '\'' +
                ", amount='" + amount + '\'' +
                ", customerId=" + customerId +
                '}';
    }
}
