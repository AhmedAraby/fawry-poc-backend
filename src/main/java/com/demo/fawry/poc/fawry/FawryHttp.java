package com.demo.fawry.poc.fawry;

import com.demo.fawry.poc.fawry.constants.FawryAPIs;
import com.demo.fawry.poc.fawry.constants.FawrySecrets;
import com.demo.fawry.poc.fawry.models.FawryReferenceNumberRequestModel;
import com.demo.fawry.poc.fawry.models.FawryReferenceNumberResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Map;

@Service
public class FawryHttp
{
    private RestTemplate restTemplate;
    public FawryHttp(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    public FawryReferenceNumberResponseModel referenceNumberPaymentRequest(FawryReferenceNumberRequestModel fawryReferenceNumberRequestModel)
    {
        // headers
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        // body
        HttpEntity<FawryReferenceNumberRequestModel> httpEntity = new HttpEntity<>(fawryReferenceNumberRequestModel, httpHeaders);

        FawryReferenceNumberResponseModel resp;
        try{
            resp = this.restTemplate.exchange(
                    FawryAPIs.REFERENCE_NUMBER_PAYMENT_API_STAGING,
                    HttpMethod.POST,
                    httpEntity,
                    FawryReferenceNumberResponseModel.class
            ).getBody();
        }

        catch(RestClientException exc)
        {
            // we need to make sure that exc is subclass of HttpClientErrorException
            System.out.println("Exception is : \n" + exc);

            HttpClientErrorException detailedExc = (HttpClientErrorException) exc;
            int statusCode = detailedExc.getRawStatusCode();
            String message = detailedExc.getMessage();  // we need to get better message, based on the status code, response body will be very useful

            System.out.println("status code \n" + statusCode);
            System.out.println("message " + message);

            throw new RuntimeException(message);
        }

        return resp;
    }
}
