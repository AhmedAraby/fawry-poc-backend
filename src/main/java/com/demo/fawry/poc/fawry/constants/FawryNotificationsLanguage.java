package com.demo.fawry.poc.fawry.constants;

public class FawryNotificationsLanguage
{

    public static final String ar = "ar-eg";
    public static final String en = "en-gb";

}
