package com.demo.fawry.poc.fawry.models;

public class FawryReferenceNumberResponseModel
{
    private String type;
    private String referenceNumber;  // transaction id in fawry system
    private String merchantRefNumber; // transaction id in our system
    private String orderAmount; // 2 decimal places     **
    private String paymentAmount; // 2 decimal places   **
    private String fawryFees; // 2 decimal places
    private String paymentMethod;
    private String orderStatus;
    private long paymentTime; // time stamp of the actual time the payment processed (does this mean customer had paid !?).     ** (yes it is zero till the customer pay)
    private String customerMobile;
    private String customerMail;
    private String customerProfileId; // customer id in my system
    private String signature; // SHA-256 of the request for Fawry to verify with the secret key shared between us
    private String statusCode; // if not 200 then we have an error
    private String statusDescription;

    public FawryReferenceNumberResponseModel() { // for jackson
    }

    public FawryReferenceNumberResponseModel(
            String type, String referenceNumber,
            String merchantRefNumber, String orderAmount,
            String paymentAmount, String fawryFees,
            String paymentMethod, String orderStatus,
            long paymentTime, String customerMobile,
            String customerMail, String customerProfileId,
            String signature, String statusCode,
            String statusDescription)
    {
        this.type = type;
        this.referenceNumber = referenceNumber;
        this.merchantRefNumber = merchantRefNumber;
        this.orderAmount = orderAmount;
        this.paymentAmount = paymentAmount;
        this.fawryFees = fawryFees;
        this.paymentMethod = paymentMethod;
        this.orderStatus = orderStatus;
        this.paymentTime = paymentTime;
        this.customerMobile = customerMobile;
        this.customerMail = customerMail;
        this.customerProfileId = customerProfileId;
        this.signature = signature;
        this.statusCode = statusCode;
        this.statusDescription = statusDescription;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getMerchantRefNumber() {
        return merchantRefNumber;
    }

    public void setMerchantRefNumber(String merchantRefNumber) {
        this.merchantRefNumber = merchantRefNumber;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getFawryFees() {
        return fawryFees;
    }

    public void setFawryFees(String fawryFees) {
        this.fawryFees = fawryFees;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public long getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(long paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerMail() {
        return customerMail;
    }

    public void setCustomerMail(String customerMail) {
        this.customerMail = customerMail;
    }

    public String getCustomerProfileId() {
        return customerProfileId;
    }

    public void setCustomerProfileId(String customerProfileId) {
        this.customerProfileId = customerProfileId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public String toString() {
        return "FawryReferenceNumberResponseModel{" +
                "type='" + type + '\'' +
                ", referenceNumber='" + referenceNumber + '\'' +
                ", merchantRefNumber='" + merchantRefNumber + '\'' +
                ", orderAmount='" + orderAmount + '\'' +
                ", paymentAmount='" + paymentAmount + '\'' +
                ", fawryFees='" + fawryFees + '\'' +
                ", paymentMethod='" + paymentMethod + '\'' +
                ", orderStatus='" + orderStatus + '\'' +
                ", paymentTime=" + paymentTime +
                ", customerMobile='" + customerMobile + '\'' +
                ", customerMail='" + customerMail + '\'' +
                ", customerProfileId='" + customerProfileId + '\'' +
                ", signature='" + signature + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", statusDescription='" + statusDescription + '\'' +
                '}';
    }
}
