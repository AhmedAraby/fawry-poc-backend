package com.demo.fawry.poc.fawry.constants;

public enum FawryTransactionStatus
{
    // POSSIBLE TRANSACTION STATUES IN FAWRY
    NEW,
    PAID,
    CANCELED,
    DELIVERED,
    REFUNDED,
    EXPIRED;

}
