package com.demo.fawry.poc.fawry.models;

import com.demo.fawry.poc.entities.Item;
import com.demo.fawry.poc.fawry.constants.FawryNotificationsLanguage;
import com.demo.fawry.poc.fawry.constants.FawryPaymentMethod;
import com.demo.fawry.poc.fawry.constants.FawrySecrets;
import com.demo.fawry.poc.utils.NumberFormater;
import com.demo.fawry.poc.utils.SignatureGenerator;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * signature will be calculated internally
 */
public class FawryReferenceNumberRequestModel
{
    private String merchantCode; // from fawry account
    private int merchantRefNum; // transaction id in my database
    private int customerProfileId; // customer id in my database
    private String paymentMethod;
    private String customerName;
    private String customerMobile;
    private String customerEmail;

    // in the from xx.xx, amount I need to get from the customer, this is my reference to make sure the customer did paid all the money
    // but in reality it just need to have 2 decimal places
    // ask about it ?
    private String amount;

//    private String currencyCode; // if currency is not included in the request how to to tell fawry that we wan to use another currency

    private long paymentExpiry; // in millie seconds
    private String description;
    private String language;
    List<Item> chargeItems = new ArrayList<>();   // mandatory to have at least 1 item
    private String signature;

    public FawryReferenceNumberRequestModel() {
    }

    public FawryReferenceNumberRequestModel(
            String merchantCode, int merchantRefNum,
            int customerProfileId, String paymentMethod,
            String customerName, String customerMobile,
            String customerEmail, String amount, //String currencyCode,
            long paymentExpiry, String description,
            String language,
            List<Item> chargeItems)
    {
        this.merchantCode = merchantCode;
        this.merchantRefNum = merchantRefNum;
        this.customerProfileId = customerProfileId;
        this.paymentMethod = paymentMethod;
        this.customerName = customerName;
        this.customerMobile = customerMobile;
        this.customerEmail = customerEmail;

        amount = NumberFormater.putAmountIn_x_xx_format(amount);
        this.amount = amount;

//        this.currencyCode = currencyCode;
        this.paymentExpiry = paymentExpiry;
        this.description = description;
        this.language = language;
        this.chargeItems = chargeItems;

        // calculate signature
        List<String> fields = Arrays.asList(
                merchantCode, String.valueOf(merchantRefNum),
                String.valueOf(customerProfileId), paymentMethod,
                amount, FawrySecrets.securityKey);
        SignatureGenerator signatureGenerator = new SignatureGenerator();
        String signature = signatureGenerator.sha256(fields);
        this.signature = signature;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public int getMerchantRefNum() {
        return merchantRefNum;
    }

    public void setMerchantRefNum(int merchantRefNum) {
        this.merchantRefNum = merchantRefNum;
    }

    public int getCustomerProfileId() {
        return customerProfileId;
    }

    public void setCustomerProfileId(int customerProfileId) {
        this.customerProfileId = customerProfileId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        amount = NumberFormater.putAmountIn_x_xx_format(amount);
        this.amount = amount;
    }

//    public String getCurrencyCode() {
//        return currencyCode;
//    }
//
//    public void setCurrencyCode(String currencyCode) {
//        this.currencyCode = currencyCode;
//    }

    public long getPaymentExpiry() {
        return paymentExpiry;
    }

    public void setPaymentExpiry(long paymentExpiry) {
        this.paymentExpiry = paymentExpiry;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<Item> getChargeItems() {
        return chargeItems;
    }

    public void setChargeItems(List<Item> chargeItems) {
        this.chargeItems = chargeItems;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "FawryReferenceNumberRequestModel{" +
                "merchantCode='" + merchantCode + '\'' +
                ", merchantRefNum=" + merchantRefNum +
                ", customerProfileId=" + customerProfileId +
                ", paymentMethod=" + paymentMethod +
                ", customerName='" + customerName + '\'' +
                ", customerMobile='" + customerMobile + '\'' +
                ", customerEmail='" + customerEmail + '\'' +
                ", amount='" + amount + '\'' +
                ", paymentExpiry=" + paymentExpiry +
                ", Description='" + description + '\'' +
                ", language=" + language +
                ", chargeItems=" + chargeItems +
                ", signature='" + signature + '\'' +
                '}';
    };
}
