package com.demo.fawry.poc.fawry.constants;

public class FawryAPIs
{
    public static final String REFERENCE_NUMBER_PAYMENT_API_STAGING = "https://atfawry.fawrystaging.com/ECommerceWeb/Fawry/payments/charge";
    public static final String REFERENCE_NUMBER_PAYMENT_API_PRODUCTION = "https://www.atfawry.com/ECommerceWeb/Fawry/payments/charge";
}
