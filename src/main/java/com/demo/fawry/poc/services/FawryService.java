package com.demo.fawry.poc.services;

import com.demo.fawry.poc.dao.CustomerDaoImpl;
import com.demo.fawry.poc.dao.ItemDaoImpl;
import com.demo.fawry.poc.dao.TransactionDaoImpl;
import com.demo.fawry.poc.entities.Customer;
import com.demo.fawry.poc.entities.Item;
import com.demo.fawry.poc.entities.Transaction;
import com.demo.fawry.poc.fawry.FawryHttp;
import com.demo.fawry.poc.fawry.constants.FawryPaymentMethod;
import com.demo.fawry.poc.fawry.constants.FawrySecrets;
import com.demo.fawry.poc.fawry.constants.FawryTransactionStatus;
import com.demo.fawry.poc.fawry.models.FawryItemModel;
import com.demo.fawry.poc.fawry.models.FawryReferenceNumberRequestModel;
import com.demo.fawry.poc.fawry.models.FawryReferenceNumberResponseModel;
import com.demo.fawry.poc.models.ReferenceNumberRequestModel;
import com.demo.fawry.poc.models.ReferenceNumberResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FawryService
{
    private CustomerDaoImpl customerDao;
    private ItemDaoImpl itemDao;
    private TransactionDaoImpl transactionDao;
    private FawryHttp fawryHttp;

    public FawryService(
            CustomerDaoImpl customerDao,
            ItemDaoImpl itemDao,
            TransactionDaoImpl transactionDao,
            FawryHttp fawryHttp)
    {
        this.customerDao = customerDao;
        this.itemDao = itemDao;
        this.transactionDao = transactionDao;
        this.fawryHttp = fawryHttp;
    }

    private long getDaysinMs(int daysCount)
    {
        return daysCount * 24 * 60 * 60 * 1000;
    }

    @Transactional
    public ReferenceNumberResponseModel doReferenceNumberPaymentRequest(
            ReferenceNumberRequestModel referenceNumberRequestModel)
    {
        // extract data from the http client
        int customerId = referenceNumberRequestModel.getCustomerId();
        String amount = referenceNumberRequestModel.getAmount();
        String language = referenceNumberRequestModel.getLanguage();

        // customer data
        Customer customer = customerDao.get(customerId);

        // create the transaction
        String description = "payment testing for our poc";
        long paymentExpiry = new Date().getTime() + getDaysinMs(1);
        Transaction transaction= new Transaction(
                FawryPaymentMethod.PAYATFAWRY.name(), amount,
                new Timestamp(paymentExpiry), description,
                language,

                // this data should be set when we get the response from fawry
                null,
                0, null,
                null,null,
                null, null,
                null, null,
                null);
        transaction.setCustomer(customer);
        transactionDao.save(transaction);

        // create item locally for now
        Item item = new Item("item # 1 ", 2, 25);
        item.setTransaction(transaction);
        itemDao.save(item);
        List<Item> items = new ArrayList<>();
        items.add(item);

        // do the http request to fawry
        FawryReferenceNumberRequestModel fawryReferenceNumberRequestModel = new FawryReferenceNumberRequestModel(
                FawrySecrets.merchantCode, transaction.getId(),
                customer.getId(), FawryPaymentMethod.PAYATFAWRY.name(),
                customer.getName(), customer.getPhoneNumber(),
                customer.getEmail(), amount,
                paymentExpiry, description,
                language, items);
        System.out.println("request model " + fawryReferenceNumberRequestModel);

        FawryReferenceNumberResponseModel fawryReferenceNumberResponseModel =
                fawryHttp.referenceNumberPaymentRequest(fawryReferenceNumberRequestModel);

        // verify the response signature
        // update the transaction data with the results comming from fawry
        transaction.setRequestSignature(fawryReferenceNumberRequestModel.getSignature()); // from request model
        System.out.println("response model " + fawryReferenceNumberResponseModel);

        // handle request failure
        if(fawryReferenceNumberResponseModel.getStatusCode().equals("200") == false)
            throw new RuntimeException(
                    "fawry request failed msg " +
                    fawryReferenceNumberResponseModel.getStatusDescription() +
                    "status is : " +
                    fawryReferenceNumberResponseModel.getStatusCode());

        // from response model
        transaction.setReferenceNumber(Integer.valueOf(fawryReferenceNumberResponseModel.getReferenceNumber()));
        transaction.setOrderAmount(fawryReferenceNumberResponseModel.getOrderAmount());
        transaction.setPaymentAmount(fawryReferenceNumberResponseModel.getPaymentAmount());
        transaction.setFawryFees(fawryReferenceNumberResponseModel.getFawryFees());
        transaction.setOrderStatus(fawryReferenceNumberResponseModel.getOrderStatus());

        if(fawryReferenceNumberResponseModel.getOrderStatus() == FawryTransactionStatus.PAID.name())
            transaction.setPaymentTime(new Timestamp(fawryReferenceNumberResponseModel.getPaymentTime()));

        transaction.setResponseSignature(fawryReferenceNumberResponseModel.getSignature());
        transaction.setStatusCode(fawryReferenceNumberResponseModel.getStatusCode());
        transaction.setStatusDescription(fawryReferenceNumberResponseModel.getStatusDescription());
        transactionDao.update(transaction);

        // prepare the http response to return

        ReferenceNumberResponseModel referenceNumberResponseModel = new ReferenceNumberResponseModel(
                fawryReferenceNumberResponseModel.getReferenceNumber(),
                paymentExpiry);
        return referenceNumberResponseModel;
    }

}
