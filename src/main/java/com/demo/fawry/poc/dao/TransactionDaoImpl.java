package com.demo.fawry.poc.dao;

import com.demo.fawry.poc.entities.Item;
import com.demo.fawry.poc.entities.Transaction;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class TransactionDaoImpl
{
    private EntityManager entityManager;

    public TransactionDaoImpl(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    private Session getSession()
    {
        return this.entityManager.unwrap(Session.class);
    }

    public void save(Transaction transaction)
    {
        Session session = this.getSession();
        session.save(transaction);
    }

    public void update(Transaction transaction)
    {
        Session session = this.getSession();
        session.update(transaction);
    }

    public void delete(Transaction transaction)
    {
        Session session = this.getSession();
        session.delete(transaction);
    }

    public Transaction get(int id)
    {
        Session session = this.getSession();
        return session.get(Transaction.class, id);
    }

}
