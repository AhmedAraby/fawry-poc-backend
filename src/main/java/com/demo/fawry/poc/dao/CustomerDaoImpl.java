package com.demo.fawry.poc.dao;

import com.demo.fawry.poc.entities.Customer;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class CustomerDaoImpl
{
    private EntityManager entityManager;

    public CustomerDaoImpl(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    private Session getSession()
    {
        return this.entityManager.unwrap(Session.class);
    }

    public void save(Customer customer)
    {
        Session session = this.getSession();
        session.save(customer);
    }

    public void update(Customer customer)
    {
        Session session = this.getSession();
        session.update(customer);
    }

    public void delete(Customer customer)
    {
        Session session = this.getSession();
        session.delete(customer);
    }

    public Customer get(int id)
    {
        Session session = this.getSession();
        return session.get(Customer.class, id);
    }

}
