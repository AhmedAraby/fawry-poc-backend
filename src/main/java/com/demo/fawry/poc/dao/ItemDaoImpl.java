package com.demo.fawry.poc.dao;

import com.demo.fawry.poc.entities.Customer;
import com.demo.fawry.poc.entities.Item;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class ItemDaoImpl
{
    private EntityManager entityManager;

    public ItemDaoImpl(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    private Session getSession()
    {
        return this.entityManager.unwrap(Session.class);
    }

    public void save(Item item)
    {
        Session session = this.getSession();
        session.save(item);
    }

    public void update(Item item)
    {
        Session session = this.getSession();
        session.update(item);
    }

    public void delete(Item item)
    {
        Session session = this.getSession();
        session.delete(item);
    }

    public Item get(int id)
    {
        Session session = this.getSession();
        return session.get(Item.class, id);
    }
}
