package com.demo.fawry.poc.controllers;

import com.demo.fawry.poc.entities.Item;
import com.demo.fawry.poc.fawry.FawryHttp;
import com.demo.fawry.poc.fawry.constants.FawryNotificationsLanguage;
import com.demo.fawry.poc.fawry.constants.FawryPaymentMethod;
import com.demo.fawry.poc.fawry.constants.FawrySecrets;
import com.demo.fawry.poc.fawry.models.FawryItemModel;
import com.demo.fawry.poc.fawry.models.FawryReferenceNumberRequestModel;
import com.demo.fawry.poc.fawry.models.FawryReferenceNumberResponseModel;
import com.demo.fawry.poc.models.ReferenceNumberRequestModel;
import com.demo.fawry.poc.models.ReferenceNumberResponseModel;
import com.demo.fawry.poc.services.FawryService;
import com.demo.fawry.poc.utils.SignatureGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@RestController
public class FawryPaymentController
{
    @Autowired
    FawryService fawryService;

    @CrossOrigin(origins = {"http://localhost:4200"})
    @PostMapping("/payment/fawry/ref")
    public ResponseEntity<ReferenceNumberResponseModel> doReferenceNumberPaymentRequest(
             @RequestBody  ReferenceNumberRequestModel referenceNumberRequestModel
    )
    {
        // here validation is supposed to happen
        ReferenceNumberResponseModel referenceNumberResponseModel =
                fawryService.doReferenceNumberPaymentRequest(referenceNumberRequestModel);
        return ResponseEntity.ok(referenceNumberResponseModel);
    }
}
