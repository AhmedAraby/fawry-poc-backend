package com.demo.fawry.poc.utils;

public class NumberFormater
{
    public static String padNumber(String num)
    {
        if(num == null)
            throw new RuntimeException("Fawry: invalid number");
        if(num == "")
            return "00";
        if(num.length()  < 2)
            return "0" + num;
        return num;
    }

    public static String putAmountIn_x_xx_format(String amount)
    {
        /**
         * amount has to have 2 decimal places
         */

        if(amount == "" || amount == null)
            throw new RuntimeException("invalid amount for fawry request ");

        String[] parts = amount.split("\\.");
        System.out.println("amount " + amount );
        System.out.println("Parts : " + parts.length);
//        String left = NumberFormater.padNumber(parts[0]);
        String left = parts[0];
        String right ;
        if(parts.length==1)
            right = padNumber("");
        else
            right = padNumber(parts[1]);
        return left + "." + right;
    }
}
