package com.demo.fawry.poc.utils;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class SignatureGenerator
{
    public String sha256(List<String> ls)
    {
        String concat = String.join("", ls);
        String sha256Hex = Hashing.sha256()
                .hashString(concat, StandardCharsets.UTF_8)
                .toString();
        return sha256Hex;
    }

    public String sha256(String str)
    {
        String sha256Hex = Hashing.sha256()
                .hashString(str, StandardCharsets.UTF_8)
                .toString();
        return sha256Hex;
    }

}
