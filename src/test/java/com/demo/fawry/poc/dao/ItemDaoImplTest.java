package com.demo.fawry.poc.dao;

import com.demo.fawry.poc.entities.Item;
import com.demo.fawry.poc.entities.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
public class ItemDaoImplTest
{
    @Autowired
    private ItemDaoImpl itemDao;
    @Autowired
    private TransactionDaoImpl transactionDao;

    @Test
    public void saveTest()
    {
        // check if orm mapping is done right
        Item item = new Item("dummy item", 103.33, 22.3);
        Transaction transaction = transactionDao.get(1);
        item.setTransaction(transaction);
        itemDao.save(item);
    }

    @Test
    public void getTest()
    {
        Item item = itemDao.get(1);
        System.out.println("item we got is " + item);
    }

}
