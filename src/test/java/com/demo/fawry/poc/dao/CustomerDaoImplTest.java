package com.demo.fawry.poc.dao;

import com.demo.fawry.poc.dao.CustomerDaoImpl;
import com.demo.fawry.poc.entities.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CustomerDaoImplTest
{

    @Autowired
    private CustomerDaoImpl customerDao;

    @Test
    public void saveTest()
    {
        Customer customer = new Customer("ahmed", "00000000001", "ahmed@gmail.com", "12345678910");
        customerDao.save(customer);
        // if not exception is throw then we did the mapping right
    }
}
