package com.demo.fawry.poc.dao;

import com.demo.fawry.poc.entities.Customer;
import com.demo.fawry.poc.entities.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Date;

@SpringBootTest
public class TransactionDaoImplTest
{
    @Autowired
    private TransactionDaoImpl transactionDao;

    @Autowired
    private CustomerDaoImpl customerDao;

    @Test
    public void saveTest()
    {
        // check that ORM mapping is done right
        long timems = new Date().getTime();
        Timestamp timestamp = new Timestamp(timems);

        Customer customer = customerDao.get(1);

        Transaction transaction = new
                Transaction(
                    "PAYATFAWARY", "12.22",
                    timestamp, "buy motor policy",
                    "eg", "asasa",
                    12, "12.02",
                    "12.03", "01.02",
                    "NEW", timestamp, "asasa",
                    "200", "success"
            );
        transaction.setCustomer(customer);
        transactionDao.save(transaction);
    }
}
